#!/bin/bash

DEFAULT_MAX_NUMBER=2048

# Is the variable that will hold the number that was guessed by the user
numberGuess=-1 # Set the numberGuessed variable to -1 to make sure that it enters the while loop

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

# If the number guessed is == to the number the computer randomly guess it will exit the while statment
# Else then it stays in the while loop and goes through the if statements to see what else it needs to print out
while(( $numberGuess != $THE_NUMBER_IM_THINKING_OF ))
do
   echo "What OK cat, I’m thinking of a number from 1 to $THE_MAX_VALUE. Make a guess:"
   read numberGuess
   if (( $numberGuess < 1 ))
   then
      echo "You must enter a number that’s >= 1"
   elif (( $numberGuess > $THE_MAX_VALUE ))
   then
      echo "You must enter a number that’s <= $THE_MAX_VALUE"
   elif (( $numberGuess < $THE_NUMBER_IM_THINKING_OF ))
   then
      echo "You No cat... the number I’m thinking of is larger than $numberGuess"
   elif (( $numberGuess > $THE_NUMBER_IM_THINKING_OF ))
   then
      echo "No cat... the number I’m thinking of is smaller than $numberGuess"
   fi
done
echo "You got me"
echo " /\\_/\\ "
echo "( o.o ) "
echo " > ^ <"
